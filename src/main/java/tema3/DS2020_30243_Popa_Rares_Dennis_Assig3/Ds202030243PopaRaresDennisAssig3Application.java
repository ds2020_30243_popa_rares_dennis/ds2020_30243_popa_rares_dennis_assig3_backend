package tema3.DS2020_30243_Popa_Rares_Dennis_Assig3;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.hessian.RpcInterface;

@SpringBootApplication
public class Ds202030243PopaRaresDennisAssig3Application {

	@Bean
	public HessianProxyFactoryBean hessianInvoker() {
		HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
		//invoker.setServiceUrl("http://localhost:8080/hessianRpc");
		invoker.setServiceUrl("https://backendrares.herokuapp.com/hessianRpc");
		invoker.setServiceInterface(RpcInterface.class);
		return invoker;
	}

	public static RpcInterface RPC;

	public static void main(String[] args) {
		ConfigurableApplicationContext context =  SpringApplication.run(Ds202030243PopaRaresDennisAssig3Application.class,args);
		RPC = context.getBean(RpcInterface.class);
	}

}

package tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos.MedicationPlanDTO;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos.MedicationTakenDTO;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos.PatientDTO;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos.UserDTO;
import tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.hessian.RpcInterface;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/main")
public class MainController {


    private final RpcInterface RPC;
    private final Gson GSON;

    @Autowired
    public MainController(RpcInterface RPC, Gson gson) {
        this.RPC = RPC;
        GSON = gson;
    }

    @GetMapping(value = "/getMedicationPlans")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> plans = new ArrayList<>();
        List<String> gsons = RPC.getMedicationPlans();
        for (String s : gsons)
            plans.add(GSON.fromJson(s, MedicationPlanDTO.class));
        return new ResponseEntity<>(plans, HttpStatus.OK);
    }

    @PostMapping(value = "/insertMedicationTaken")
    public ResponseEntity<Long> insertMedicationTaken(@Valid @RequestBody MedicationTakenDTO medicationTakenDTO) {
        String gson = GSON.toJson(medicationTakenDTO, MedicationTakenDTO.class);
        return new ResponseEntity<>(RPC.insertMedicationTaken(gson), HttpStatus.OK);
    }

    @GetMapping(value = "findMedicationPlanById/{id}")
    public ResponseEntity<MedicationPlanDTO> getMedicationPlanById(@PathVariable(name = "id") Long medicationPlanId) {
        String gson = RPC.findMedicationPlanById((long) medicationPlanId);
        MedicationPlanDTO plan = GSON.fromJson(gson, MedicationPlanDTO.class);
        return new ResponseEntity<>(plan, HttpStatus.OK);
    }

    @GetMapping(value = "findUserByEmail/{email}")
    public ResponseEntity<UserDTO> getUserByEmail(@PathVariable(name = "email") String email) {
        String gson = RPC.findUserByEmail(email);
        UserDTO user = GSON.fromJson(gson, UserDTO.class);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "findPatientByUser")
    public ResponseEntity<PatientDTO> findPatientByUser(@Valid @RequestBody UserDTO userDTO) {
        Gson g = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd").create();
        String userGson = g.toJson(userDTO, UserDTO.class);
        String patientGson = RPC.findPatientByUser(userGson);
        PatientDTO patient = g.fromJson(patientGson, PatientDTO.class);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @GetMapping(value = "/checkLogin/{email}/{password}")
    public ResponseEntity<UserDTO> checkLogin(@PathVariable(name = "email") String email,
                             @PathVariable(name = "password") String password) {
        String response = RPC.checkLogin(email,password);
        if(response == "")
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        else return new ResponseEntity<>(GSON.fromJson(response,UserDTO.class),HttpStatus.OK);
    }

    @GetMapping(value = "/getPatientByUserId/{id}")
    public ResponseEntity<PatientDTO> getPatientByUserId(@PathVariable(name = "id") Long userId){
        String response = RPC.getPatientByUserId(userId);
        return new ResponseEntity<>(GSON.fromJson(response,PatientDTO.class),HttpStatus.OK);
    }
}

package tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.hessian;

import java.io.Serializable;
import java.util.List;


public interface RpcInterface extends Serializable {
    public Long insertMedicationTaken(String medicationTakenDTO);

    public List<String> getMedicationPlans();

    public String findMedicationPlanById(Long medicationPlanId);

    public String findUserByEmail(String email);

    public String findPatientByUser(String userDTO);

    public String checkLogin(String email,String password);

    public String getPatientByUserId(Long userId);
}



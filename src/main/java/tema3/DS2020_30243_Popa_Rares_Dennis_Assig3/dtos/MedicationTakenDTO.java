package tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class MedicationTakenDTO implements Serializable {

    private Long id;
    @NotNull
    private MedicationPlanDTO medicationPlanDTO;
    @NotNull
    private Long patientId;
    @NotNull
    private boolean taken;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MedicationPlanDTO getMedicationPlanDTO() {
        return medicationPlanDTO;
    }

    public void setMedicationPlanDTO(MedicationPlanDTO medicationPlanDTO) {
        this.medicationPlanDTO = medicationPlanDTO;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }
}

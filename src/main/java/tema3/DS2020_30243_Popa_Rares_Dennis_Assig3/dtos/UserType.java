package tema3.DS2020_30243_Popa_Rares_Dennis_Assig3.dtos;

import java.io.Serializable;

public enum UserType implements Serializable {
    MEDIC,CAREGIVER,PATIENT
}
